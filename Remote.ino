#include <nRF24L01.h>
#include <SPI.h>
#include <printf.h>
#include <RF24_config.h>
#include <RF24.h>

RF24 radio(9,10);
const byte address[6] = "00001";
int xAxis, yAxis;
int button;

void setup(){
    Serial.begin(9600);

    radio.begin();
    radio.openWritingPipe(address);
    radio.setPALevel(RF24_PA_MIN);
    radio.stopListening();

    Serial.println("Sending...");
}

void loop(){
    //Read analong input for Joystick
    xAxis = analogRead(A0);
    yAxis = analogRead(A1);
    //Clamps the input to either 0 or 1
    button = constrain(analogRead(A2), 0, 1);

    //xAxis - yAxis - joystickButton
    int dataPacket[3] = {(xAxis/2), (yAxis/2), (button)};
    radio.write(&dataPacket, sizeof(dataPacket));
}